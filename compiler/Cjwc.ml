type options = { out : string; input : string }

let program_description = "\
Usage: cjwc [OPTIONS] INPUT
Options:
  --help                Display this help information
  -o <file>             Place the output into <file>
"

let parse_commandline_options args = 
	let rec parse' opt = function 
		| "-o" :: file :: rest -> parse' { opt with out = file } rest
		| "--help" :: _        -> Printf.printf "%s\n" program_description; exit 0
		| _arg :: rest         -> parse' opt rest
		| []                   -> opt
	
	in parse' { out = "out.bin"; input = "" } args

let main args = 
	let _options = parse_commandline_options args in
	Printf.fprintf stderr "Hi\n"

;;

main (Sys.argv |> Array.to_list)
