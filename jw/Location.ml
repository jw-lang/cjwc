type pos = 
	{file : string; line : int; column : int}

type region = 
	{left : pos; right : pos}

type 'a loc = 
	{ at : region; it : 'a; }

let (@@) loc' region = 
	{at = region; it = loc'}


