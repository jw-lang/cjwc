{
	open Parser
}

let digit = ['0'-'9']
let num = (digit | ['1'-'9'] digit*)

rule token = parse
	| num as s { NUM (int_of_string s) }
