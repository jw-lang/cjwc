
%{
open Syntax
open Location

let position_to_pos position =
	{ file = position.Lexing.pos_fname;
		line = position.Lexing.pos_lnum;
		column = position.Lexing.pos_cnum - position.Lexing.pos_bol
	}

let positions_to_region position1 position2 =
	{ left = position_to_pos position1;
		right = position_to_pos position2
	}

let at () =
	positions_to_region (Parsing.symbol_start_pos ()) (Parsing.symbol_end_pos ())

(*
let ati i =
	positions_to_region (Parsing.rhs_start_pos i) (Parsing.rhs_end_pos i) 
*)
%}

%token EOF
%token<int> NUM

%start expr

%type<Syntax.expr> expr

%%

expr :
	| NUM EOF
		{ IntExpr($1) @@ at () }

%%

